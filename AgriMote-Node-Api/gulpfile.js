'use strict';

const gulp = require('gulp');
const jshint = require('gulp-jshint');
const jscs = require('gulp-jscs');

const config = require('./gulp.config')();

gulp.task('lint', function () {
    return gulp
        .src(config.alljs)
        .pipe(jscs())
        .pipe(jshint())
        .pipe(jshint.reporter('jshint-stylish', {verbose: true}));
});

gulp.task('serve-dev', ['lint'], function () {
    const nodemon = require('gulp-nodemon');
    nodemon({
        script: config.start,
        ext: 'js',
        delay: 1000,
        ignore: config.ignoreFiles
    })
    .on('restart', function (files) {
        console.log('Change detected:', files);
    });
});

function startTest(test, done) {
    const mocha = require('gulp-mocha');
    gulp.src(test, {read: false})
        .pipe(mocha({reporter: 'spec'}));
    done();
}

gulp.task('test', function (done) {
    startTest('tests/field.specs.js', done);
});