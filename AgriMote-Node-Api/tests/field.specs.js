'use strict';

const supertest = require('supertest');
const settings = require('../settings.json');
const assert = require('assert');
const fs = require('fs');
const api = supertest(settings.url + ":" + settings.port);
var util = require('util');

describe('Test REST-API', function () {
    it('Should POST coords, and type', function (done) {
        var response = fs.readFileSync(process.cwd() + '/test-data/geojson.json').toString();
        var type = 'Polygon';
        var coords = [
            [
                150.347900390625,
                -26.185018250078308
            ],
            [
                152.127685546875,
                -29.82158272057499
            ],
            [
                151.14990234375,
                -25.53252846853443
            ],
            [
                148.07373046875,
                -25.403584973186703
            ],
            [
                150.347900390625,
                -26.185018250078308
            ]
        ];
        var request = {type: type, coords: coords};
        api.post('/field')
            .set('Accept','application/json')
            .send(request)
            .expect(200)
            .end(function (err, res) {
                assert.equal(res.body.type, JSON.parse(response).type);
                done();
            });
    });
    
});