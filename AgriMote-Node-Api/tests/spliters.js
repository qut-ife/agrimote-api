'use strict';

const assert = require('assert');
const fs = require('fs');

describe('Test split liam date', function () {
    it('Should split liam date', function (done) {
        var date = '2016/4/20/0/ndvi-mercator';
        var location = {
            year: '', month: '', day: '', extra:''
        };
        var oldLocation = {
            year: '2016', month: '4', day: '20', extra:'0'
        };
        var split = date.split('/');
        location.year = split[0];
        location.month = split[1];
        location.day = split[2];
        location.extra = split[3];
        
        assert.deepEqual(location, oldLocation);
        done();
    });
    it('Should split liam date objects', function (done) {
        var date = '2016/04/20/0/ndvi-mercator';
        var location = {
            year: '', month: '', day: '', extra:''
        };
        var oldLocation = {
            year: '2016', month: '4', day: '20', extra:'0'
        };
        var split = date.split('/');
        location.year = parseInt(split[0]);
        location.month = parseInt(split[1]);
        location.day = parseInt(split[2]);
        location.extra = parseInt(split[3]);

        assert.deepEqual(location, oldLocation);
        done();
    });
});
