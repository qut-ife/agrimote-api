'use strict';

const assert = require('assert');
const xyz = require('../app/xyz');
const settings = require('../settings.json');

describe('Test XYZ conversion', function () {
    it('write xyz file', function (done) {
        this.timeout(10000);
        let fileLocation = settings.fileDump;
        let fileName = '_4326_chiba';
        xyz(fileLocation, fileName, function (res) {
            assert.equal('', res);
            done();
        });
    });
});
