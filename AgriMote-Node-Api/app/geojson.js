'use strict';

module.exports = function (coords, type) {
    return {
        "type": "FeatureCollection",
        "features": [
        {
            "type": "Feature",
            "properties": {},
            "id":0,
            "geometry": {
                "type": type || "Polygon",
                "coordinates": [coords] || []
            }
        }
        ]
    };
};