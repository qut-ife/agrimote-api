'use strict';

/*
    gdal_translate -of xyz 4326_clipped.tif 4326_clipped.xyz
 */
const settings = require('../settings.json');
//const log = require('./logger')();

module.exports = function (clippedTif, xyz) {
    var cmd = settings.gdal_translate;
    var args = ['-of','xyz', clippedTif, xyz];
    //log.debug('gdal_translate; cmd:' + cmd + ', args:' + args);
    return {cmd, args};
};