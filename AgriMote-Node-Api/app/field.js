'use strict';

const Geojson = require('./geojson');
const log = require('./logger')();
//const util = require('util');
const crypto = require('crypto');
const cmd = require('./cmd');
const settings = require('../settings.json');
const Gdalwarp = require('./gdalwarp');
const Gdal_translate = require('./gdal_translate');
const XYZ = require('./xyz');

module.exports.generate = function (coords, type, tile) {
  return new Promise(function(resolve, reject) {
      try {
          var geojson = Geojson(coords, type);
          //console.log(util.inspect(geojson, {showHidden: false, depth: null}));
          var fileName = crypto.randomBytes(48).toString('hex');
          var gdalwarpIn = settings.tiles + tile.folder + 'NDVIcalc.tif';
          var gdalwarp = Gdalwarp(fileName, geojson, gdalwarpIn);
          cmd
              .run(gdalwarp.cmd, gdalwarp.args)
              .then((gwarp) => {
                  log.debug("gwarp: " + gwarp);
                  var tif = settings.fileDump + fileName + '.tif';
                  var xyz = settings.fileDump + fileName + '.xyz';
                  var gdal_translate = Gdal_translate(tif, xyz);
                  return cmd.run(gdal_translate.cmd, gdal_translate.args);
              })
              .then((gtrans) => {
                  log.debug("gtrans: " + gtrans);
                  XYZ(settings.fileDump, fileName)
                      .then(() => {
                          var result = {
                              res: 0,
                              file: fileName,
                              trans: gtrans
                          };
                          resolve(result);
                      })
                      .catch(()=>{
                          var result = {
                              res: 1,
                              file: fileName,
                              trans: gtrans
                          };
                          resolve(result);
                      })
              })
              .catch((error) => {
                  log.error(error.message);
                  throw error;
              });
      }catch(error){
          log.error(error.message);
          throw error;
      }
  });
};



