'use strict';

var spawn = require('child_process').spawn;
const log = require('./logger')();

module.exports.run = (cmd, args) => {
    return new Promise((resolve, reject) => {

        log.debug(cmd + ' ' +args.join(' '));
        var child = spawn(cmd, args);
        var resp = "";

        child.stdout.on('data', function (buffer) {
            resp += buffer.toString();
        });

        child.stdout.on('end', function() {
            log.debug("end: " + resp);
        });

        child.on('close', function (code) {
            log.debug(cmd + ' process exited with code '+ code);
            if(code === 1){
                reject(new Error(cmd + ' process exited with code '+ code))
            }else{
                resolve(code)
            }
        });

        child.on('error', function(error){
            log.error(cmd + ': ' +error.message);
            reject(new Error(error.message));
        });
    })
};