'use strict';
const settings = require('../settings.json');

module.exports = function (tileName, date) {
    var folder = [tileName, date.year,date.month, date.day, date.extra, ''].join(settings.slash);
    return {
        tile: tileName,
        ymd: [date.year,date.month, date.day, date.extra].join(settings.slash),
        extra: date.extra,
        folder: folder
    };
};