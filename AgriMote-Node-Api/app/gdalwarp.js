'use strict';

//gdalwarp -t_srs EPSG:4326 -crop_to_cutline -cutline clipped.tif 4326_clipped.tif
const settings = require('../settings.json');
const fs = require('fs');
const log = require('./logger')();

module.exports = function (fileName, geojson, gdalwarpIn) {
    var cmd = settings.gdalwarp;
    var location = settings.fileDump;
    var geojsonFile = location + fileName + '.geojson';
    writeFile(geojsonFile, JSON.stringify(geojson));
    var gdalwarpOut = location + fileName + '.tif';
    var args = ['-t_srs','EPSG:4326','-crop_to_cutline','-cutline', geojsonFile, gdalwarpIn, gdalwarpOut];

    function writeFile(fileName, content){
        try{
            fs.writeFileSync(fileName, content, 'utf8');
        }catch(e){
            log.error(e.message);
            log.error("cannot write file "+ fileName);
            throw e.message;
        }
    }

    return {cmd, args};
};
