'use strict';

const express = require('express');
const bodyParser = require('body-parser');
const log = require('./logger')();
const settings = require('../settings.json');
const field = require('./field');
const Tile = require('./tile');
const moment = require('moment');

let allowCrossDomain = function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,POST');//Maybe PUT, DELETE?
    res.header('Access-Control-Allow-Headers', 'Content-Type');
    next();
};

var app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(allowCrossDomain);

app.get('/', function (req, res) {
    res.status(200).json('Hi');
});

//Generate field with gdalwarp and gdal_translate
app.post('/field/generate', function (req, res) {
    //Because it will convert to string maybe wrap around try
    try {
        //console.log(req.body);
        var coords = typeof(req.body.coords) === 'object' ? req.body.coords : JSON.parse(req.body.coords);
        var type = req.body.type;
        var tileName = req.body.tileName;
        var date = req.body.date;
        var tile = Tile(tileName, date);
        field
            .generate(coords, type, tile)
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (error) {
                log.error(error);
                res.status(500).json({message: "Error parsing coordinates"});
            });
    }catch(error){
        log.error("Error parsing coordinates");
        res.status(500).json({message: "Error parsing coordinates"});
    }
});

//Download field file
app.get('/field/download/:file/:areaName', function (req, res) {
    try {
        var fieldReq = req.params.file;
        var areaName = req.params.areaName || 'field';
        if(!fieldReq) {
            res.status(400).json({message: "File Request Empty"});
        }
        var date = moment().format("MM_DD_HH_MM");
        //res.setHeader('Content-type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        let ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
        log.debug(settings.fileDump + fieldReq + '.csv');
        res.download(settings.fileDump + fieldReq + '.csv', areaName + '_' + date + '.csv', function(err) {
            if (err) {
                //alert user and try again
                console.log(err);
                log.error('Download failed, error:' + err + ', from: ' + ip + ', ' + fieldReq);
                if(res.headersSent){
                    log.error(res.headersSent);
                }
                res.status(500).json({message: err});
            } else {
                //delete file if it has completed download!!
                log.info('Download, IP: ' + ip + ' , file: ' + fieldReq);
            }
        });
    }catch(error){
        log.error(error);
        res.status(500).json({message: error});
    }
});

var server = app.listen(process.env.PORT || settings.port, function(){
    log.info('Api listening on ', server.address().port);
});
