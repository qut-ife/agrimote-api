'use strict';

const fs = require('fs');
const log = require('./logger')();

module.exports = function (fileLocation, fileName) {
    return new Promise((resolve, reject) => {
        try {
            var file = fs.readFileSync(fileLocation + fileName + '.xyz').toString();
            var lines = file.split('\n');
            var outArray = [];
            var cur = '';
            var prev = '';
            var i = 1;
            var yArray = [];

            yArray.push(' ');
            yArray.push(lines[0].split(' ')[0]);
            while (cur === prev) {
                cur = lines[i].split(' ')[1];
                yArray.push(lines[i].split(' ')[0]);
                prev = lines[i - 1].split(' ')[1];
                i++;
            }
            yArray = yArray.slice(0, yArray.length - 1);
            outArray.push(yArray.join(','));

            var maxY = Math.round((lines.length) / (i - 1));
            var maxX = (i - 1);
            for (var y = 0; y < maxY; y++) {
                var xArray = [];
                for (var x = 0; x < maxX; x++) {
                    if (x === 0) {
                        xArray.push(lines[y * maxX + x].split(' ')[1]);
                    }
                    xArray.push(lines[y * maxX + x].split(' ')[2]);
                }
                outArray.push(xArray.join(','));
            }

            var out = fs.createWriteStream(fileLocation + fileName + '.csv');
            outArray.forEach(function (line) {
                out.write(line + '\n');
            });
            out.end(function () {
                resolve();
            });
            out.on('error', function (error) {
                out.end();
                log.error(error.message);
                reject(error)
            });
        }catch (error){
            log.error(error.message);
            reject(error)
        }
    })
};