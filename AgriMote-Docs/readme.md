###Documentation

How to crop tiles

https://blog.snap.uaf.edu/2013/03/25/clipping-rasters-with-geojson-polygons/

crop files with gdalwarp

gdalwarp -dstnodata 255 -co COMPRESS=DEFLATE -of GTiff -r lanczos -crop_to_cutline -cutline polygon.json input.tif clipped.tif

`gdalwarp t_srs EPSG:4326 clipped.tif 4326_clipped.tif`

`gdal_translate -of xyz 4326_clipped.tif 4326_clipped.xyz`

