AgriMote Wiki
```
$ git clone https://qut-ife@bitbucket.org/qut-ife/agrimote-api.git/wiki
```
SuperOverlays

https://developers.google.com/kml/articles/raster

####For Mac install gdal

- http://www.kyngchaos.com/software:frameworks
	- Install DMG
- `export PATH=/Library/Frameworks/GDAL.framework/Programs:$PATH`
- check installation
	- `gdalinfo --version`

####GDAL:

- Nuget PM> Install-Package GDAL
- Install http://www.gisinternals.com/ x32 or x64
- Add to system environment variables GDAL and gdal-data
- If you run into problems this may be because of what are you building in VS an x64 or a 32 gdal

####API:
- Nuget PM> Install-Package
  - Microsoft.AspNet.WebApi
  - Microsoft.AspNet.WebApi.Owin
  - Microsoft.Owin.Host.SystemWeb
  - Microsoft.Owin.Security.Jwt
  - Microsoft.Owin.Cors

Installing GDAL linux

- sudo apt-get install gdal-bin python-gdal

Install dotnetcore on debian 8

- sudo apt-get install libunwind8-dev
- sudo apt-get install gettext
- sudo apt-get install curl
- curl -sSL https://raw.githubusercontent.com/dotnet/cli/rel/1.0.0-preview1/scripts/obtain/dotnet-install.sh | bash /dev/stdin --version 1.0.0-preview1-002702 --install-dir ~/dotnet
- sudo ln -s ~/dotnet/dotnet /usr/local/bin


####Install Node.js in debian

- TODO: Instalation instructions

####Node Api Process

#####Field Process

1. Send coordinates to node.js api with type, polygon and circle
1. Clip with gdalwarp with input geojson and t_srs EPSG:4326
1. gdaltranslate to convert to -of xyz (xyz table)
1. Manual manipulation of xyz table into col,row,val

modify settings.json to point apps

#####Apache Node.js

`sudo a2enmod proxy`

`sudo a2enmod proxy_http`

`sudo vim /etc/apache2/sites-available/griphus.conf`

Add:

``ProxyPass /api-g http://localhost:3000``

``ProxyPassReverse /api-g http://localhost:3000``

