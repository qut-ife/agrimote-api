'use strict';

var gulp = require('gulp'),
    concat = require('gulp-concat'),
    minify = require('gulp-minify'),
    less = require('gulp-less'),
    fs = require('fs');


gulp.task('build', function() {
    gulp
        .src(['scripts/**/*'])
        .pipe(concat('app.js'))
        .pipe(gulp.dest('js'));
});
gulp.task('concat', function(){
    gulp.src([
            'vendor/jquery/dist/jquery.min.js',
            'vendor/bootstrap/dist/js/bootstrap.min.js',
            'vendor/angular/angular.min.js',
            'vendor/angular-route/angular-route.min.js',
            'vendor/angular-animate/angular-animate.min.js',
            'vendor/angular-carousel/dist/angular-carousel.min.js',
            'vendor/angular-touch/angular-touch.min.js'
        ])
        .pipe(concat('lib.js'))
        .pipe(gulp.dest('js'));
});
gulp.task('compress', function() {
    gulp.src('js/app.js')
        .pipe(minify({
            exclude: [],
            ignoreFiles: []
        }))
        .pipe(gulp.dest('js'));
    gulp.src(['less/main.less'])
        .pipe(less())
        .pipe(gulp.dest('css'));
});
gulp.task('watch', function () {
    gulp.watch(['scripts/**/*.js','js/*.js','less/*.less'], ['build', 'compress']);
});

gulp.task('prod', ['build-prod'], function () {
    const scp = require('gulp-scp2');
    gulp
        .src(['prod/**/*'])
        .pipe(scp({
            host: 'griphus.com.au',
            username: 'admin',
            privateKey: fs.readFileSync('/Users/Moy/Dropbox/Trabajo/eResearch/amazon-aws/agrimap/heemssh.pem').toString(),
            dest: '/var/www/griphus/app/'
        }))
        .on('error', function(err) {
            console.log(err);
        });
});

gulp.task('build-prod', function (done) {
    gulp
        .src(['*css/**/*', '*images/**/*', '*js/**/*', 'index.html', '*pages/**/*', '*templates/**/*'])
        .pipe(gulp.dest('prod/'));
    done();
});

gulp.task('push-to-prod', ['prod', 'build-prod']);

gulp.task('default', ['watch', 'concat']);