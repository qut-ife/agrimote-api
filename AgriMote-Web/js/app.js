'use strict';
angular.module('app', ['ngRoute', 'ngAnimate', 'angular-carousel']);
angular.module('app').config(['$provide', '$routeProvider',
    function ($provide, $routeProvider) {
        $provide
            .decorator("$exceptionHandler", ["$delegate",
                function ($delegate) {
                    return function (exception, cause) {
                        console.log(exception.message);
                        $delegate(exception, cause);
                    };
                }
            ]);
        $routeProvider
            .when('/', {
                controller: 'apiController',
                templateUrl: 'pages/api.html'
            });

    }
]);
angular.module('app').controller('aboutController', ['$scope',
    function($scope){
    }]);
angular.module('app').controller('apiController',
    ['$scope', '$location', '$compile', 'dataService', 'sharedProperties', 'configService', 'toolsService',
    function ($scope, $location, $compile, dataService, sharedProperties, configService, toolsService) {

        $scope.config = {};
        $scope.start = true;
        $scope.map = {};
        $scope.mapCenter = { lat: -23.932508, lng: 148.505645 };
        $scope.mapBounds = [
            {lat:-12.960368, lng:152.390811},//1
            {lat:-12.960368, lng:114.993349},//2
            {lat:-42.308777, lng:114.993349},//3
            {lat:-42.308777, lng:152.390811}//4
        ];
        $scope.toolsToggled = true;
        $scope.mapZoom = 4;
        $scope.polygons = [];
        $scope.data = {};
        $scope.polygonsOnScreen = false;
        $scope.tools = {
            title: 'Select a Tile'
        };

        $scope.tile = {
            date: '',
            name: '',
            loaded: false,
            zoom: {
                min:0, max:0, best:0
            },
            bestZoom: 6,
            coords: {},
            selected: false,
            location:{}
        };

        $scope.tileHover = {
            name: 'my tile',
            show: false
        };

        $scope.showDateMenu = false;
        $scope.tileInfo = {};
        //$scope.disablePolygons = false;
        $scope.disablePolygonInfo = false;
        var clickListener, mouseOverListener;
        $scope.enableFieldMapper = false;

        $scope.tabSelected = 1;
        $scope.currentTile = {};
        $scope.currentZoom = 0;

        var addListenersOnPolygon = function (polygon) {
            clickListener = google.maps.event.addListener(polygon, 'click', function (event) {
                if ($scope.currentTile == null || ($scope.currentTile.id !== polygon.name)) {
                    dataService
                        .getTileInfo($scope.config, polygon.name)
                        .then(function (response) {
                            $scope.tileInfo = response.data;
                            $scope.tileInfo.dateFormatArray = toolsService.preetyDates($scope.tileInfo.dates);
                            $scope.tile.name = polygon.name;
                            $scope.tile.coords = polygon.coords;
                            $scope.tile.selected = true;
                            $scope.tools.title = 'Select a Date';
                            $scope.showDateMenu = true;
                            var currentTile = {};
                            if($scope.start) {
                                $scope.setBounds($scope.tile.coords);
                                //set first date available
                                var lastDay = $scope.tileInfo.dateFormatArray.length-1;
                                $scope.tileDateSelected = $scope.tileInfo.dateFormatArray[lastDay];
                                currentTile = $scope.tileInfo.dates[lastDay];
                                currentTile.dateFormat = $scope.tileInfo.dateFormatArray[lastDay].dateFormat;
                                $scope.loadTile(currentTile);
                                $scope.currentTile.id = polygon.name;
                                $scope.start = false;
                            } else if($scope.currentTile){
                                currentTile = toolsService.findIdByDate($scope.tileInfo.dates, $scope.currentTile.date);
                                if(currentTile.index >= 0 && currentTile.index != null) {
                                    $scope.tileDateSelected = $scope.tileInfo.dateFormatArray[currentTile.index];
                                    currentTile.dateFound.dateFormat = $scope.tileDateSelected.dateFormat;
                                    $scope.loadTile(currentTile.dateFound);
                                    $scope.currentTile.id = polygon.name;
                                    $scope.start = false;
                                }else{
                                    $scope.removeOverlay();
                                    alert('No satellite data for this date, please select a different one');
                                }
                            }
                            $scope.setPolygonOptions({strokeOpacity:1, strokeWeight:2, fillOpacity:0, strokeColor:'red', zIndex: 1000},
                                {strokeOpacity:1, strokeWeight:1,fillOpacity:0, strokeColor:'black', zIndex: 1},
                                polygon.index);
                        }, function(e){
                            if(e && e.status === 404){
                                $scope.tools.title = 'Tile Not Found';
                                $scope.showDateMenu = false;
                                throw "Problem: " + e.status + ", " + e.statusText;
                            }else {
                                throw "Problem loading tile";
                            }
                        });
                }
            });
            mouseOverListener = google.maps.event.addListener(polygon, 'mouseover', function (event) {
                if(!$scope.disablePolygonInfo) {
                    var self = this;
                    $scope.$apply(function () {
                        //$scope.tileHover.style = {top: event.latLng.lng() + 'px', left: event.latLng.lat() + 'px'};
                        $scope.tileHover.show = true;
                        $scope.tileHover.name = self.name;
                        $scope.tileHover.lastImage = '';
                    });
                }
            });
            google.maps.event.addListener(polygon, 'mouseout', function (event) {
                $scope.$apply(function () {
                    $scope.tileHover.show = false;
                });
            });
        };

        $scope.loadTile = function (info) {
            //console.log(info);
            $scope.disablePolygonInfo = true;
            //$scope.disablePolygons = true;
            $scope.tile.date = info.date;
            $scope.tile.dateFormat = info.dateFormat;
            $scope.tile.dataCoverage = info.dataCoverage;
            $scope.tile.cloudCoverage = info.cloudCoverage;
            $scope.tile.location = toolsService.convertLiamDate(info.date);
            $scope.currentTile = info;
            $scope.setTile();
        };
        
        var initMap = function(){
            $scope.tileHover.show = false;
            dataService
                .getTiles($scope.config)
                .then(function (response) {
                    $scope.map = new google.maps.Map(document.getElementById('map_canvas'), {
                        zoom: $scope.mapZoom,
                        center: $scope.mapCenter,
                        scaleControl: true,
                        zoomControl: true,
                        zoomControlOptions: {
                            position: google.maps.ControlPosition.LEFT_BOTTOM
                        },
                        streetViewControl: false,
                        fullscreenControl: false
                    });
                    $scope.data = response.data;
                    $scope.setPolygons();
                    $scope.setBounds($scope.mapBounds);
                    $scope.map.addListener('zoom_changed', function() {
                        $scope.currentZoom =  $scope.map.getZoom();
                    });
                });
        };

        $scope.setPolygons = function() {
            $scope.polygonsOnScreen = true;
            for (var i = 0; i < $scope.data.length; i++) {
                $scope.polygon = new google.maps.Polygon({
                    paths: $scope.data[i].coords,
                    strokeColor: '#1f1f2e',
                    strokeOpacity: 0.2,
                    strokeWeight: 1,
                    fillColor: '#2E8B57',
                    fillOpacity: 0.3
                });
                $scope.polygon.index = i;
                $scope.polygon.name = $scope.data[i].name;
                $scope.polygon.coords = $scope.data[i].coords;
                $scope.polygon.setMap($scope.map);
                addListenersOnPolygon($scope.polygon);
                $scope.polygons.push($scope.polygon);
            }
        };

        $scope.setTile = function () {
            $scope.removeOverlay();
            //$scope.polygonsOnScreen = true;
            $scope.tabSelected = 1;
            //$scope.togglePolygons();
            $scope.tile.folder = $scope.tileInfo.folder;
            if ($scope.start) {
                $scope.setBounds($scope.tile.coords);
                $scope.setZoom($scope.tileInfo.zoom.min);
                $scope.start = false;
            }
            $scope.map.overlayMapTypes.clear();
            $scope.tools.title = '';
            $scope.imageMapType = new google.maps.ImageMapType({
                getTileUrl: function (coord, zoom) {
                    $scope.$apply(function () {
                        $scope.tile.zoom = zoom;
                    });
                    var url = [$scope.config.origin + $scope.config.sat + '/' + $scope.tileInfo.folder + '/' + $scope.tile.date + '/',
                        zoom, '/', coord.x, '/', coord.y, '.png'].join('');
                    //console.log(url);
                    return url;
                },
                tileSize: new google.maps.Size(256, 256)
            });

            $scope.map.overlayMapTypes.push($scope.imageMapType);
            $scope.tile.loaded = true;
            if ($scope.start) {
                $scope.setZoom($scope.tileInfo.zoom.min);
                $scope.start = false;
            }
        };

        $scope.removeOverlay = function(){
            $scope.map.overlayMapTypes.clear();
        };
        
        $scope.restoreOverlay = function(){
            $scope.map.overlayMapTypes.clear();
            $scope.map.overlayMapTypes.push($scope.imageMapType);
        };

        $scope.togglePolygons = function () {
            $scope.polygonsOnScreen = !$scope.polygonsOnScreen;
            if($scope.polygonsOnScreen) {
                $scope.polygons.forEach(function (p) {
                    p.setMap($scope.map);
                    addListenersOnPolygon(p);
                });
            }else{
                $scope.tileHover.show = false;
                $scope.disablePolygonInfo = false;
                $scope.polygons.forEach(function(p) {
                    p.setMap(null);
                });
            }
        };

        $scope.setZoom = function (zoom) {
            $scope.map.setZoom(zoom);
        };

        $scope.setBounds = function (coords) {
            var latlngbounds = new google.maps.LatLngBounds();
            for (var i = 0; i < coords.length; i++) {
                latlngbounds.extend(new google.maps.LatLng(coords[i].lat, coords[i].lng));
            }
            $scope.map.fitBounds(latlngbounds);
        };

        $scope.clear = function () {
            //$scope.disablePolygons = false;
            $scope.disablePolygonInfo = false;
            addListenersOnPolygon($scope.polygons);
            $scope.removeOverlay();
            $scope.showDateMenu = false;
            $scope.tile.loaded = false;
            if(!$scope.polygonsOnScreen){
                $scope.polygonsOnScreen = false;
                $scope.togglePolygons();
            }
            $scope.start = true;
            $scope.tools.title = 'Select a Tile';
            $scope.tools.subTitle = '';
            $scope.tile.selected = false;
            var options = {strokeColor: '#1f1f2e',strokeOpacity: 0.2,strokeWeight: 1,fillColor: '#2E8B57',fillOpacity: 0.3};
            $scope.setPolygonOptions(null, options, null);
            $scope.setBounds($scope.mapBounds);
        };

        $scope.selectTile = function () {
            $scope.$apply(function () {
                $scope.loaded = false;
                $scope.showDateMenu = true;
                $scope.setZoom($scope.tile.bestZoom);
            });
        };

        $scope.setPolygonOptions = function(option, other, index) {
            $scope.polygons.forEach(function (polygon, i) {
                if(i === index) {
                    polygon.setOptions(option);
                }else{
                    polygon.setOptions(other);
                }
            })
        };

        $scope.toggleTools = function () {
            $scope.toolsToggled = !$scope.toolsToggled;
        };

        var init = function () {
            configService
                .getConfig()
                .then(function(response) {
                    $scope.config = response.data;
                })
                .then(initMap);
        };
        init();
    }
]);
angular.module('app').controller('appController',
    ['$scope', '$location', 'dataService', 'sharedProperties',
    function($scope){
        $scope.title = 'Griphus';
        $scope.description = 'Griphus ; another description';
}]);
angular.module('app').controller('contactController', ['$scope',
    function($scope){
    }]);
angular.module('app').controller('headerController', ['$scope', '$location',
    function ($scope, $location) {
        $scope.references = [
            { url: '#/', title: 'Home' },
            { url: '#/about', title: 'About'},
            { url: '#/contact', title: 'Contact'}

        ];
        $scope.getClass = function (path) {
            return ($location.path().substr(0, path.length) === path.substring(1)) ? 'active' : '';
        };
        $(document).on('click','.navbar-collapse.in',function(e) {
            if( $(e.target).is('a') && $(e.target).attr('class') != 'dropdown-toggle' ) {
                $(this).collapse('hide');
            }
        });
    }
]);
angular.module('app').controller('homeController', ['$location',
    function ($location) {
        /*$scope.slides = [
            {image: 'images/img00.jpg', description: 'Image 00'},
            {image: 'images/img01.jpg', description: 'Image 01'},
            {image: 'images/img02.jpg', description: 'Image 02'},
            {image: 'images/img03.jpg', description: 'Image 03'}
        ];*/
        //temporary relocation
        $location.path('/api');
    }]);
angular.module('app').controller('loginController', ['$scope', '$location',
    function ($scope, $location) {
        $scope.login = function () {
            $location.path('/api');
        }
    }]);
angular.module('app').directive('polyShape', ['$compile', '$timeout', '$filter', 'fieldService', 'configService',
    function ($compile, $timeout, $filter, fieldService, configService) {
        return{
            template:
                '<div></div>',
            link: function (scope, element, attrs) {
                scope.polygonDrawings = [];
                var drawingId = 0;
                scope.drawingManager = new google.maps.drawing.DrawingManager({
                    drawingMode: google.maps.drawing.OverlayType.POLYGON,
                    drawingControl: true,
                    drawingControlOptions: {
                        position: google.maps.ControlPosition.LEFT_BOTTOM,
                        drawingModes: [
                            //google.maps.drawing.OverlayType.CIRCLE,
                            google.maps.drawing.OverlayType.POLYGON
                        ]
                    },
                    circleOptions: {
                        fillColor: '#1f1f2e',
                        fillOpacity: 0.5,
                        strokeWeight: 3,
                        clickable: false,
                        editable: true,
                        zIndex: 1
                    }
                });
                scope.fieldMap = function () {
                    scope.enableFieldMapper = !scope.enableFieldMapper;
                    if(scope.enableFieldMapper) {
                        scope.drawingManager.setMap(scope.map);
                    }else{
                        scope.drawingManager.setMap(null);
                    }
                };

                google.maps.event.addListener(scope.drawingManager, 'polygoncomplete', function (polygon) {
                    //google.maps.event.addListener(polygon, 'click', setSelection);
                    var coordinates = polygon.getPath().getArray();
                    var config, coordsArray = [];
                    coordinates.forEach(function (c) {
                        coordsArray.push([c.lng(),c.lat()])
                    });
                    coordsArray.push([coordinates[0].lng(),coordinates[0].lat()]);                    
                    polygon.details = {name: 'my area ' + drawingId++, type:'Polygon', coords: coordsArray, url:'', processing:true};
                    scope.polygonDrawings.push(polygon);
                    configService
                        .getConfig()
                        .then(function (response) {
                            config = response.data;
                            fieldService
                                .postField(config, {type:'Polygon',
                                    coords: coordsArray,
                                    tileName: scope.tile.folder,
                                    date: scope.tile.location})
                                .then(function (response) {
                                    getPolygonIndex(polygon.$$hashKey, function (index) {
                                        if(index >= 0){
                                            scope.polygonDrawings[index].details.processing = false;
                                            scope.polygonDrawings[index].details.file = response.data.file;
                                            if(response.data.file){
                                                scope.polygonDrawings[index].details.url = fieldService.download(config, scope.polygonDrawings[index]);
                                            }else{
                                                alert('Drawing not found.');
                                            }
                                        }
                                    });
                                })
                                .catch(function (error) {
                                    alert('Drawing too dificult, try another.');
                                    throw error.statusText;
                                })
                        })
                        .catch(function (error) {
                            alert('Could not connect to service.');
                            throw error;
                        });
                });
                google.maps.event.addListener(scope.drawingManager, 'circlecomplete', function(circle) {
                    var radius = circle.getRadius();
                    alert('Liam get me some coords with this radius = ' + radius);
                });

                scope.trashDrawing = function(hashKey){
                    getPolygonIndex(hashKey, function (index) {
                        if(index >= 0) {
                            scope.polygonDrawings[index].setMap(null);
                            scope.polygonDrawings.splice(index, 1);
                        }
                    });
                };

                var getPolygonIndex = function (hashKey, cb) {
                    $filter('filter')(scope.polygonDrawings, function(polygon) {
                        if(polygon.$$hashKey === hashKey){
                            cb(scope.polygonDrawings.indexOf(polygon));
                        }else{
                            cb(-1)
                        }
                    });
                }

            }
        }
    }]);
angular.module('app').directive('searchBar', ['$compile', 'dataService', 'toolsService',
    function ($compile, dataService, toolsService) {
    return{
        scope: {
            map: '='
        },
        template:
            '<div ng-cloak>'+
            '<input id="searchInput" class="searchInput controls" placeholder="Search" value="">'+
            '<img id="poweredBy" src="images/powered_by_google_on_non_white.png" class="poweredBy" />'+
            '</div>',
        link: function (scope, element, attrs) {
            scope.$watch("map",function (n, o) {
                if(n != o) {
                    var el = angular.element('<div class="map_canvas"/>');
                    var input = document.getElementById('searchInput');
                    scope.map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
                    var powered = document.getElementById('poweredBy');
                    scope.map.controls[google.maps.ControlPosition.TOP_LEFT].push(powered);
                    var searchBox = new google.maps.places.SearchBox(input);

                    google.maps.event.addListener(searchBox, 'places_changed', function () {
                        scope.places = searchBox.getPlaces();
                        if (scope.places.length == 0) {
                            return;
                        }
                        var markers = [];
                        markers.forEach(function (m) {
                            m.setMap(null)
                        });
                        var bounds = new google.maps.LatLngBounds();
                        for (var p = 0, place; place = scope.places[p]; p++) {
                            var image = {
                                url: place.icon,
                                size: new google.maps.Size(71, 71),
                                origin: new google.maps.Point(0, 0),
                                anchor: new google.maps.Point(17, 34),
                                scaledSize: new google.maps.Size(25, 25)
                            };
                            var marker = new google.maps.Marker({
                                map: scope.map,
                                icon: image,
                                title: place.name,
                                position: place.geometry.location,
                                zIndex: 99999999
                            });
                            markers.push(marker);
                            //bounds.extend(place.geometry.location);
                            loadTileinMarker(marker, place);
                            addListenerOnMarker(marker, place);
                        }
                        //TODO: find the Tile that is inside these bounds
                        //scope.map.fitBounds(bounds);
                    });

                    scope.triggerSearch = function () {
                        var input = document.getElementById('searchInput');
                        google.maps.event.trigger(input, 'focus');
                        google.maps.event.trigger(input, 'keydown', {
                            keyCode: 13
                        });
                    };

                    $compile(el)(scope);
                    element.append(el);

                    var addListenerOnMarker = function (marker, place) {
                        google.maps.event.addListener(marker, 'click', function(event){
                            loadTileinMarker(marker, place);
                        });
                    };

                    var loadTileinMarker = function(marker, place) {
                        //var bounds = new google.maps.LatLngBounds();
                        //TODO: Zoom or bound only to the tile zoom
                        //bounds.extend(place.geometry.location);
                        //scope.map.fitBounds(bounds);
                        //then set min zoom
                        //console.log(scope.$parent.tileInfo.max);
                        //scope.map.setZoom(scope.$parent.tileInfo.min);
                        //scope.$parent.setZoom(scope.$parent.tileInfo.min);
                        var noPolygon = true;
                        scope.$parent.polygons.map(function (polygon) {
                            var isWithinPolygon = polygon.containsLatLng(place.geometry.location);
                            if(isWithinPolygon){
                                noPolygon = false;
                                dataService
                                    .getTileInfo(scope.$parent.config, polygon.name)
                                    .then(function (response) {
                                        scope.$parent.tileInfo = response.data;
                                        scope.$parent.tileInfo.dateFormatArray = toolsService.preetyDates(scope.$parent.tileInfo.dates);
                                        scope.$parent.tile.name = polygon.name;
                                        scope.$parent.tile.coords = polygon.coords;
                                        scope.$parent.tile.selected = true;
                                        scope.$parent.tools.title = 'Select a Date';
                                        scope.$parent.showDateMenu = true;
                                        scope.$parent.startTile = true;
                                        var currentTile = {};

                                        if (scope.$parent.start) {
                                            scope.$parent.setBounds(scope.$parent.tile.coords);
                                            //set first date available
                                            var lastDay = scope.$parent.tileInfo.dateFormatArray.length-1;
                                            scope.$parent.tileDateSelected = scope.$parent.tileInfo.dateFormatArray[lastDay];
                                            currentTile = scope.$parent.tileInfo.dates[lastDay];
                                            currentTile.dateFormat = scope.$parent.tileInfo.dateFormatArray[lastDay].dateFormat;
                                            scope.$parent.loadTile(currentTile);
                                            scope.$parent.currentTile.id = polygon.name;
                                            scope.map.setZoom(scope.$parent.tileInfo.zoom.best);
                                            scope.$parent.start = false;
                                        } else if(scope.$parent.currentTile){
                                            currentTile = toolsService.findIdByDate(scope.$parent.tileInfo.dates, scope.$parent.currentTile.date);
                                            if(currentTile.index >= 0 && currentTile.index != null) {
                                                scope.$parent.tileDateSelected = scope.$parent.tileInfo.dateFormatArray[currentTile.index];
                                                currentTile.dateFound.dateFormat = scope.$parent.tileDateSelected.dateFormat;
                                                scope.$parent.loadTile(currentTile.dateFound);
                                                scope.$parent.currentTile.id = polygon.name;
                                                scope.$parent.start = false;
                                            }else{
                                                scope.$parent.removeOverlay();
                                                alert('No satellite data for this date, please select a different one');
                                            }
                                        }
                                        scope.$parent.setPolygonOptions({
                                                strokeOpacity: 1,
                                                strokeWeight: 2,
                                                fillOpacity: 0,
                                                strokeColor: 'red',
                                                zIndex: 1000
                                            },
                                            {
                                                strokeOpacity: 1,
                                                strokeWeight: 1,
                                                fillOpacity: 0,
                                                strokeColor: 'black',
                                                zIndex: 1
                                            },
                                            polygon.index);
                                        scope.map.setCenter(marker.getPosition());
                                    });
                            }
                        });
                        if(noPolygon) {
                            alert('No satellite data for this area');
                        }
                    }
                }
            });
        }
    }
}]);
angular.module('app').service('configService', ['$http',
    function ($http) {
        var getConfig = function () {
            return $http.get('/api/conf');
        };
        return {
            getConfig: getConfig
        }
    }]);
angular.module('app').factory('dataService', ['$http',
    function ($http) {

        var getTiles = function (config) {
            var tiles = config.origin + config.api + '/tiles';
            return $http.get(tiles);
        };
        
        var getTileInfo = function (config, name) {
            var tileInfo = config.origin + config.api + '/tileInfo/' + name;
            return $http.get(tileInfo);
        };

        return {
            getTiles: getTiles,
            getTileInfo: getTileInfo
        }
    }
]);
angular.module('app').factory('fieldService', ['$http',
    function ($http) {

        var postField = function (config, data) {
            var postConfig = {
                headers : {
                    'Content-Type': 'application/json'
                }
            };
            var field = config.compute + config.field;
            return $http.post(field, data, postConfig);
        };

        var download = function (config, polygon) {
            return config.compute + config.fieldDownload + '/' +  polygon.details.file + '/' + polygon.details.name;
        };

        return {
            postField: postField,
            download: download
        }
    }
]);
angular.module('app').service('sharedProperties', [
    function () {
        var hashtable = {};
        return {
            printValues: function(){
                console.log(hashtable)
            },
            setValue: function (key, value) {
                hashtable[key] = value;
            },
            getValue: function (key) {
                return hashtable[key];
            }
        }
    }
]);
angular.module('app').service('toolsService', [
    function () {
        var convertLiamDate = function (date) {
            var location = {
                year:'', month:'', day:''
            };
            var split = date.split('/');
            location.year = parseInt(split[0]);
            location.month = parseInt(split[1]);
            location.day = parseInt(split[2]);
            location.extra = parseInt(split[3]);

            return location;
        };
        var preetyDates = function (dateArray) {
            var preetyDateArrays = [];
            dateArray.forEach(function (ele) {
                var split = ele.date.split('/');
                var dateFormat = split[2] +'/' + getMonth(split[1]) + '/'+ split[0];
                var oDate = {
                    date: ele.date,
                    dateJS: new Date(split[0], split[1] - 1, split[2]),
                    dateFormat: dateFormat,
                    dataCoverage: ele.dataCoverage,
                    cloudCoverage: ele.cloudCoverage 
                };
                preetyDateArrays.push(oDate);
            });
            return preetyDateArrays;
        };
        function getMonth(m){
            var month = ['','Jan','Feb','March','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
            return month[m] || '';
        }
        var findIdByDate = function (infoDates, date) {
            var dateFound = {}, index = null;
            infoDates.forEach(function(info, i){
                if(info.date === date){
                    dateFound = info;
                    index = i;
                }
            });
            return {dateFound: dateFound, index:index}
        };
        return{
            convertLiamDate: convertLiamDate,
            preetyDates: preetyDates,
            findIdByDate: findIdByDate
        }
    }
]);
