"use strict";

describe('api', function () {

    beforeEach(module('app'));
    window.jasmine.DEFAULT_TIMEOUT_INTERVAL = 10000;

    var $controller;

    beforeEach(inject(function(_$controller_){
        $controller = _$controller_;
    }));

    describe('init', function () {
        var $scope = {};
        var controller;
        beforeEach(function (done) {
            controller = $controller('apiController', { $scope: $scope });
            $scope.init(function () {
                done();
            });
        });
        it('It should init', function () {
            console.log($scope.polygons.index);
            expect($scope.polygons.index).toBeGreaterThan(1)
        });
    });
});