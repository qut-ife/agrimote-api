angular.module('app').controller('headerController', ['$scope', '$location',
    function ($scope, $location) {
        $scope.references = [
            { url: '#/', title: 'Home' },
            { url: '#/about', title: 'About'},
            { url: '#/contact', title: 'Contact'}

        ];
        $scope.getClass = function (path) {
            return ($location.path().substr(0, path.length) === path.substring(1)) ? 'active' : '';
        };
        $(document).on('click','.navbar-collapse.in',function(e) {
            if( $(e.target).is('a') && $(e.target).attr('class') != 'dropdown-toggle' ) {
                $(this).collapse('hide');
            }
        });
    }
]);