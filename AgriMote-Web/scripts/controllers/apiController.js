angular.module('app').controller('apiController',
    ['$scope', '$location', '$compile', 'dataService', 'sharedProperties', 'configService', 'toolsService',
    function ($scope, $location, $compile, dataService, sharedProperties, configService, toolsService) {

        $scope.config = {};
        $scope.start = true;
        $scope.map = {};
        $scope.mapCenter = { lat: -23.932508, lng: 148.505645 };
        $scope.mapBounds = [
            {lat:-12.960368, lng:152.390811},//1
            {lat:-12.960368, lng:114.993349},//2
            {lat:-42.308777, lng:114.993349},//3
            {lat:-42.308777, lng:152.390811}//4
        ];
        $scope.toolsToggled = true;
        $scope.mapZoom = 4;
        $scope.polygons = [];
        $scope.data = {};
        $scope.polygonsOnScreen = false;
        $scope.tools = {
            title: 'Select a Tile'
        };

        $scope.tile = {
            date: '',
            name: '',
            loaded: false,
            zoom: {
                min:0, max:0, best:0
            },
            bestZoom: 6,
            coords: {},
            selected: false,
            location:{}
        };

        $scope.tileHover = {
            name: 'my tile',
            show: false
        };

        $scope.showDateMenu = false;
        $scope.tileInfo = {};
        //$scope.disablePolygons = false;
        $scope.disablePolygonInfo = false;
        var clickListener, mouseOverListener;
        $scope.enableFieldMapper = false;

        $scope.tabSelected = 1;
        $scope.currentTile = {};
        $scope.currentZoom = 0;

        var addListenersOnPolygon = function (polygon) {
            clickListener = google.maps.event.addListener(polygon, 'click', function (event) {
                if ($scope.currentTile == null || ($scope.currentTile.id !== polygon.name)) {
                    dataService
                        .getTileInfo($scope.config, polygon.name)
                        .then(function (response) {
                            $scope.tileInfo = response.data;
                            $scope.tileInfo.dateFormatArray = toolsService.preetyDates($scope.tileInfo.dates);
                            $scope.tile.name = polygon.name;
                            $scope.tile.coords = polygon.coords;
                            $scope.tile.selected = true;
                            $scope.tools.title = 'Select a Date';
                            $scope.showDateMenu = true;
                            var currentTile = {};
                            if($scope.start) {
                                $scope.setBounds($scope.tile.coords);
                                //set first date available
                                var lastDay = $scope.tileInfo.dateFormatArray.length-1;
                                $scope.tileDateSelected = $scope.tileInfo.dateFormatArray[lastDay];
                                currentTile = $scope.tileInfo.dates[lastDay];
                                currentTile.dateFormat = $scope.tileInfo.dateFormatArray[lastDay].dateFormat;
                                $scope.loadTile(currentTile);
                                $scope.currentTile.id = polygon.name;
                                $scope.start = false;
                            } else if($scope.currentTile){
                                currentTile = toolsService.findIdByDate($scope.tileInfo.dates, $scope.currentTile.date);
                                if(currentTile.index >= 0 && currentTile.index != null) {
                                    $scope.tileDateSelected = $scope.tileInfo.dateFormatArray[currentTile.index];
                                    currentTile.dateFound.dateFormat = $scope.tileDateSelected.dateFormat;
                                    $scope.loadTile(currentTile.dateFound);
                                    $scope.currentTile.id = polygon.name;
                                    $scope.start = false;
                                }else{
                                    $scope.removeOverlay();
                                    alert('No satellite data for this date, please select a different one');
                                }
                            }
                            $scope.setPolygonOptions({strokeOpacity:1, strokeWeight:2, fillOpacity:0, strokeColor:'red', zIndex: 1000},
                                {strokeOpacity:1, strokeWeight:1,fillOpacity:0, strokeColor:'black', zIndex: 1},
                                polygon.index);
                        }, function(e){
                            if(e && e.status === 404){
                                $scope.tools.title = 'Tile Not Found';
                                $scope.showDateMenu = false;
                                throw "Problem: " + e.status + ", " + e.statusText;
                            }else {
                                throw "Problem loading tile";
                            }
                        });
                }
            });
            mouseOverListener = google.maps.event.addListener(polygon, 'mouseover', function (event) {
                if(!$scope.disablePolygonInfo) {
                    var self = this;
                    $scope.$apply(function () {
                        //$scope.tileHover.style = {top: event.latLng.lng() + 'px', left: event.latLng.lat() + 'px'};
                        $scope.tileHover.show = true;
                        $scope.tileHover.name = self.name;
                        $scope.tileHover.lastImage = '';
                    });
                }
            });
            google.maps.event.addListener(polygon, 'mouseout', function (event) {
                $scope.$apply(function () {
                    $scope.tileHover.show = false;
                });
            });
        };

        $scope.loadTile = function (info) {
            //console.log(info);
            $scope.disablePolygonInfo = true;
            //$scope.disablePolygons = true;
            $scope.tile.date = info.date;
            $scope.tile.dateFormat = info.dateFormat;
            $scope.tile.dataCoverage = info.dataCoverage;
            $scope.tile.cloudCoverage = info.cloudCoverage;
            $scope.tile.location = toolsService.convertLiamDate(info.date);
            $scope.currentTile = info;
            $scope.setTile();
        };
        
        var initMap = function(){
            $scope.tileHover.show = false;
            dataService
                .getTiles($scope.config)
                .then(function (response) {
                    $scope.map = new google.maps.Map(document.getElementById('map_canvas'), {
                        zoom: $scope.mapZoom,
                        center: $scope.mapCenter,
                        scaleControl: true,
                        zoomControl: true,
                        zoomControlOptions: {
                            position: google.maps.ControlPosition.LEFT_BOTTOM
                        },
                        streetViewControl: false,
                        fullscreenControl: false
                    });
                    $scope.data = response.data;
                    $scope.setPolygons();
                    $scope.setBounds($scope.mapBounds);
                    $scope.map.addListener('zoom_changed', function() {
                        $scope.currentZoom =  $scope.map.getZoom();
                    });
                });
        };

        $scope.setPolygons = function() {
            $scope.polygonsOnScreen = true;
            for (var i = 0; i < $scope.data.length; i++) {
                $scope.polygon = new google.maps.Polygon({
                    paths: $scope.data[i].coords,
                    strokeColor: '#1f1f2e',
                    strokeOpacity: 0.2,
                    strokeWeight: 1,
                    fillColor: '#2E8B57',
                    fillOpacity: 0.3
                });
                $scope.polygon.index = i;
                $scope.polygon.name = $scope.data[i].name;
                $scope.polygon.coords = $scope.data[i].coords;
                $scope.polygon.setMap($scope.map);
                addListenersOnPolygon($scope.polygon);
                $scope.polygons.push($scope.polygon);
            }
        };

        $scope.setTile = function () {
            $scope.removeOverlay();
            //$scope.polygonsOnScreen = true;
            $scope.tabSelected = 1;
            //$scope.togglePolygons();
            $scope.tile.folder = $scope.tileInfo.folder;
            if ($scope.start) {
                $scope.setBounds($scope.tile.coords);
                $scope.setZoom($scope.tileInfo.zoom.min);
                $scope.start = false;
            }
            $scope.map.overlayMapTypes.clear();
            $scope.tools.title = '';
            $scope.imageMapType = new google.maps.ImageMapType({
                getTileUrl: function (coord, zoom) {
                    $scope.$apply(function () {
                        $scope.tile.zoom = zoom;
                    });
                    var url = [$scope.config.origin + $scope.config.sat + '/' + $scope.tileInfo.folder + '/' + $scope.tile.date + '/',
                        zoom, '/', coord.x, '/', coord.y, '.png'].join('');
                    //console.log(url);
                    return url;
                },
                tileSize: new google.maps.Size(256, 256)
            });

            $scope.map.overlayMapTypes.push($scope.imageMapType);
            $scope.tile.loaded = true;
            if ($scope.start) {
                $scope.setZoom($scope.tileInfo.zoom.min);
                $scope.start = false;
            }
        };

        $scope.removeOverlay = function(){
            $scope.map.overlayMapTypes.clear();
        };
        
        $scope.restoreOverlay = function(){
            $scope.map.overlayMapTypes.clear();
            $scope.map.overlayMapTypes.push($scope.imageMapType);
        };

        $scope.togglePolygons = function () {
            $scope.polygonsOnScreen = !$scope.polygonsOnScreen;
            if($scope.polygonsOnScreen) {
                $scope.polygons.forEach(function (p) {
                    p.setMap($scope.map);
                    addListenersOnPolygon(p);
                });
            }else{
                $scope.tileHover.show = false;
                $scope.disablePolygonInfo = false;
                $scope.polygons.forEach(function(p) {
                    p.setMap(null);
                });
            }
        };

        $scope.setZoom = function (zoom) {
            $scope.map.setZoom(zoom);
        };

        $scope.setBounds = function (coords) {
            var latlngbounds = new google.maps.LatLngBounds();
            for (var i = 0; i < coords.length; i++) {
                latlngbounds.extend(new google.maps.LatLng(coords[i].lat, coords[i].lng));
            }
            $scope.map.fitBounds(latlngbounds);
        };

        $scope.clear = function () {
            //$scope.disablePolygons = false;
            $scope.disablePolygonInfo = false;
            addListenersOnPolygon($scope.polygons);
            $scope.removeOverlay();
            $scope.showDateMenu = false;
            $scope.tile.loaded = false;
            if(!$scope.polygonsOnScreen){
                $scope.polygonsOnScreen = false;
                $scope.togglePolygons();
            }
            $scope.start = true;
            $scope.tools.title = 'Select a Tile';
            $scope.tools.subTitle = '';
            $scope.tile.selected = false;
            var options = {strokeColor: '#1f1f2e',strokeOpacity: 0.2,strokeWeight: 1,fillColor: '#2E8B57',fillOpacity: 0.3};
            $scope.setPolygonOptions(null, options, null);
            $scope.setBounds($scope.mapBounds);
        };

        $scope.selectTile = function () {
            $scope.$apply(function () {
                $scope.loaded = false;
                $scope.showDateMenu = true;
                $scope.setZoom($scope.tile.bestZoom);
            });
        };

        $scope.setPolygonOptions = function(option, other, index) {
            $scope.polygons.forEach(function (polygon, i) {
                if(i === index) {
                    polygon.setOptions(option);
                }else{
                    polygon.setOptions(other);
                }
            })
        };

        $scope.toggleTools = function () {
            $scope.toolsToggled = !$scope.toolsToggled;
        };

        var init = function () {
            configService
                .getConfig()
                .then(function(response) {
                    $scope.config = response.data;
                })
                .then(initMap);
        };
        init();
    }
]);