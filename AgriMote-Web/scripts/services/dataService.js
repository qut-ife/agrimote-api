angular.module('app').factory('dataService', ['$http',
    function ($http) {

        var getTiles = function (config) {
            var tiles = config.origin + config.api + '/tiles';
            return $http.get(tiles);
        };
        
        var getTileInfo = function (config, name) {
            var tileInfo = config.origin + config.api + '/tileInfo/' + name;
            return $http.get(tileInfo);
        };

        return {
            getTiles: getTiles,
            getTileInfo: getTileInfo
        }
    }
]);