angular.module('app').service('toolsService', [
    function () {
        var convertLiamDate = function (date) {
            var location = {
                year:'', month:'', day:''
            };
            var split = date.split('/');
            location.year = parseInt(split[0]);
            location.month = parseInt(split[1]);
            location.day = parseInt(split[2]);
            location.extra = parseInt(split[3]);

            return location;
        };
        var preetyDates = function (dateArray) {
            var preetyDateArrays = [];
            dateArray.forEach(function (ele) {
                var split = ele.date.split('/');
                var dateFormat = split[2] +'/' + getMonth(split[1]) + '/'+ split[0];
                var oDate = {
                    date: ele.date,
                    dateJS: new Date(split[0], split[1] - 1, split[2]),
                    dateFormat: dateFormat,
                    dataCoverage: ele.dataCoverage,
                    cloudCoverage: ele.cloudCoverage 
                };
                preetyDateArrays.push(oDate);
            });
            return preetyDateArrays;
        };
        function getMonth(m){
            var month = ['','Jan','Feb','March','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
            return month[m] || '';
        }
        var findIdByDate = function (infoDates, date) {
            var dateFound = {}, index = null;
            infoDates.forEach(function(info, i){
                if(info.date === date){
                    dateFound = info;
                    index = i;
                }
            });
            return {dateFound: dateFound, index:index}
        };
        return{
            convertLiamDate: convertLiamDate,
            preetyDates: preetyDates,
            findIdByDate: findIdByDate
        }
    }
]);
