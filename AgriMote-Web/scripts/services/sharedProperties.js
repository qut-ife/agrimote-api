﻿angular.module('app').service('sharedProperties', [
    function () {
        var hashtable = {};
        return {
            printValues: function(){
                console.log(hashtable)
            },
            setValue: function (key, value) {
                hashtable[key] = value;
            },
            getValue: function (key) {
                return hashtable[key];
            }
        }
    }
]);