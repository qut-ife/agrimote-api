angular.module('app').factory('fieldService', ['$http',
    function ($http) {

        var postField = function (config, data) {
            var postConfig = {
                headers : {
                    'Content-Type': 'application/json'
                }
            };
            var field = config.compute + config.field;
            return $http.post(field, data, postConfig);
        };

        var download = function (config, polygon) {
            return config.compute + config.fieldDownload + '/' +  polygon.details.file + '/' + polygon.details.name;
        };

        return {
            postField: postField,
            download: download
        }
    }
]);