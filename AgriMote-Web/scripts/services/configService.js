﻿angular.module('app').service('configService', ['$http',
    function ($http) {
        var getConfig = function () {
            return $http.get('/api/conf');
        };
        return {
            getConfig: getConfig
        }
    }]);