angular.module('app').directive('polyShape', ['$compile', '$timeout', '$filter', 'fieldService', 'configService',
    function ($compile, $timeout, $filter, fieldService, configService) {
        return{
            template:
                '<div></div>',
            link: function (scope, element, attrs) {
                scope.polygonDrawings = [];
                var drawingId = 0;
                scope.drawingManager = new google.maps.drawing.DrawingManager({
                    drawingMode: google.maps.drawing.OverlayType.POLYGON,
                    drawingControl: true,
                    drawingControlOptions: {
                        position: google.maps.ControlPosition.LEFT_BOTTOM,
                        drawingModes: [
                            //google.maps.drawing.OverlayType.CIRCLE,
                            google.maps.drawing.OverlayType.POLYGON
                        ]
                    },
                    circleOptions: {
                        fillColor: '#1f1f2e',
                        fillOpacity: 0.5,
                        strokeWeight: 3,
                        clickable: false,
                        editable: true,
                        zIndex: 1
                    }
                });
                scope.fieldMap = function () {
                    scope.enableFieldMapper = !scope.enableFieldMapper;
                    if(scope.enableFieldMapper) {
                        scope.drawingManager.setMap(scope.map);
                    }else{
                        scope.drawingManager.setMap(null);
                    }
                };

                google.maps.event.addListener(scope.drawingManager, 'polygoncomplete', function (polygon) {
                    //google.maps.event.addListener(polygon, 'click', setSelection);
                    var coordinates = polygon.getPath().getArray();
                    var config, coordsArray = [];
                    coordinates.forEach(function (c) {
                        coordsArray.push([c.lng(),c.lat()])
                    });
                    coordsArray.push([coordinates[0].lng(),coordinates[0].lat()]);                    
                    polygon.details = {name: 'my area ' + drawingId++, type:'Polygon', coords: coordsArray, url:'', processing:true};
                    scope.polygonDrawings.push(polygon);
                    configService
                        .getConfig()
                        .then(function (response) {
                            config = response.data;
                            fieldService
                                .postField(config, {type:'Polygon',
                                    coords: coordsArray,
                                    tileName: scope.tile.folder,
                                    date: scope.tile.location})
                                .then(function (response) {
                                    getPolygonIndex(polygon.$$hashKey, function (index) {
                                        if(index >= 0){
                                            scope.polygonDrawings[index].details.processing = false;
                                            scope.polygonDrawings[index].details.file = response.data.file;
                                            if(response.data.file){
                                                scope.polygonDrawings[index].details.url = fieldService.download(config, scope.polygonDrawings[index]);
                                            }else{
                                                alert('Drawing not found.');
                                            }
                                        }
                                    });
                                })
                                .catch(function (error) {
                                    alert('Drawing too dificult, try another.');
                                    throw error.statusText;
                                })
                        })
                        .catch(function (error) {
                            alert('Could not connect to service.');
                            throw error;
                        });
                });
                google.maps.event.addListener(scope.drawingManager, 'circlecomplete', function(circle) {
                    var radius = circle.getRadius();
                    alert('Liam get me some coords with this radius = ' + radius);
                });

                scope.trashDrawing = function(hashKey){
                    getPolygonIndex(hashKey, function (index) {
                        if(index >= 0) {
                            scope.polygonDrawings[index].setMap(null);
                            scope.polygonDrawings.splice(index, 1);
                        }
                    });
                };

                var getPolygonIndex = function (hashKey, cb) {
                    $filter('filter')(scope.polygonDrawings, function(polygon) {
                        if(polygon.$$hashKey === hashKey){
                            cb(scope.polygonDrawings.indexOf(polygon));
                        }else{
                            cb(-1)
                        }
                    });
                }

            }
        }
    }]);