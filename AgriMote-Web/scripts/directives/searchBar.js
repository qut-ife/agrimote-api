angular.module('app').directive('searchBar', ['$compile', 'dataService', 'toolsService',
    function ($compile, dataService, toolsService) {
    return{
        scope: {
            map: '='
        },
        template:
            '<div ng-cloak>'+
            '<input id="searchInput" class="searchInput controls" placeholder="Search" value="">'+
            '<img id="poweredBy" src="images/powered_by_google_on_non_white.png" class="poweredBy" />'+
            '</div>',
        link: function (scope, element, attrs) {
            scope.$watch("map",function (n, o) {
                if(n != o) {
                    var el = angular.element('<div class="map_canvas"/>');
                    var input = document.getElementById('searchInput');
                    scope.map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
                    var powered = document.getElementById('poweredBy');
                    scope.map.controls[google.maps.ControlPosition.TOP_LEFT].push(powered);
                    var searchBox = new google.maps.places.SearchBox(input);

                    google.maps.event.addListener(searchBox, 'places_changed', function () {
                        scope.places = searchBox.getPlaces();
                        if (scope.places.length == 0) {
                            return;
                        }
                        var markers = [];
                        markers.forEach(function (m) {
                            m.setMap(null)
                        });
                        var bounds = new google.maps.LatLngBounds();
                        for (var p = 0, place; place = scope.places[p]; p++) {
                            var image = {
                                url: place.icon,
                                size: new google.maps.Size(71, 71),
                                origin: new google.maps.Point(0, 0),
                                anchor: new google.maps.Point(17, 34),
                                scaledSize: new google.maps.Size(25, 25)
                            };
                            var marker = new google.maps.Marker({
                                map: scope.map,
                                icon: image,
                                title: place.name,
                                position: place.geometry.location,
                                zIndex: 99999999
                            });
                            markers.push(marker);
                            //bounds.extend(place.geometry.location);
                            loadTileinMarker(marker, place);
                            addListenerOnMarker(marker, place);
                        }
                        //TODO: find the Tile that is inside these bounds
                        //scope.map.fitBounds(bounds);
                    });

                    scope.triggerSearch = function () {
                        var input = document.getElementById('searchInput');
                        google.maps.event.trigger(input, 'focus');
                        google.maps.event.trigger(input, 'keydown', {
                            keyCode: 13
                        });
                    };

                    $compile(el)(scope);
                    element.append(el);

                    var addListenerOnMarker = function (marker, place) {
                        google.maps.event.addListener(marker, 'click', function(event){
                            loadTileinMarker(marker, place);
                        });
                    };

                    var loadTileinMarker = function(marker, place) {
                        //var bounds = new google.maps.LatLngBounds();
                        //TODO: Zoom or bound only to the tile zoom
                        //bounds.extend(place.geometry.location);
                        //scope.map.fitBounds(bounds);
                        //then set min zoom
                        //console.log(scope.$parent.tileInfo.max);
                        //scope.map.setZoom(scope.$parent.tileInfo.min);
                        //scope.$parent.setZoom(scope.$parent.tileInfo.min);
                        var noPolygon = true;
                        scope.$parent.polygons.map(function (polygon) {
                            var isWithinPolygon = polygon.containsLatLng(place.geometry.location);
                            if(isWithinPolygon){
                                noPolygon = false;
                                dataService
                                    .getTileInfo(scope.$parent.config, polygon.name)
                                    .then(function (response) {
                                        scope.$parent.tileInfo = response.data;
                                        scope.$parent.tileInfo.dateFormatArray = toolsService.preetyDates(scope.$parent.tileInfo.dates);
                                        scope.$parent.tile.name = polygon.name;
                                        scope.$parent.tile.coords = polygon.coords;
                                        scope.$parent.tile.selected = true;
                                        scope.$parent.tools.title = 'Select a Date';
                                        scope.$parent.showDateMenu = true;
                                        scope.$parent.startTile = true;
                                        var currentTile = {};

                                        if (scope.$parent.start) {
                                            scope.$parent.setBounds(scope.$parent.tile.coords);
                                            //set first date available
                                            var lastDay = scope.$parent.tileInfo.dateFormatArray.length-1;
                                            scope.$parent.tileDateSelected = scope.$parent.tileInfo.dateFormatArray[lastDay];
                                            currentTile = scope.$parent.tileInfo.dates[lastDay];
                                            currentTile.dateFormat = scope.$parent.tileInfo.dateFormatArray[lastDay].dateFormat;
                                            scope.$parent.loadTile(currentTile);
                                            scope.$parent.currentTile.id = polygon.name;
                                            scope.map.setZoom(scope.$parent.tileInfo.zoom.best);
                                            scope.$parent.start = false;
                                        } else if(scope.$parent.currentTile){
                                            currentTile = toolsService.findIdByDate(scope.$parent.tileInfo.dates, scope.$parent.currentTile.date);
                                            if(currentTile.index >= 0 && currentTile.index != null) {
                                                scope.$parent.tileDateSelected = scope.$parent.tileInfo.dateFormatArray[currentTile.index];
                                                currentTile.dateFound.dateFormat = scope.$parent.tileDateSelected.dateFormat;
                                                scope.$parent.loadTile(currentTile.dateFound);
                                                scope.$parent.currentTile.id = polygon.name;
                                                scope.$parent.start = false;
                                            }else{
                                                scope.$parent.removeOverlay();
                                                alert('No satellite data for this date, please select a different one');
                                            }
                                        }
                                        scope.$parent.setPolygonOptions({
                                                strokeOpacity: 1,
                                                strokeWeight: 2,
                                                fillOpacity: 0,
                                                strokeColor: 'red',
                                                zIndex: 1000
                                            },
                                            {
                                                strokeOpacity: 1,
                                                strokeWeight: 1,
                                                fillOpacity: 0,
                                                strokeColor: 'black',
                                                zIndex: 1
                                            },
                                            polygon.index);
                                        scope.map.setCenter(marker.getPosition());
                                    });
                            }
                        });
                        if(noPolygon) {
                            alert('No satellite data for this area');
                        }
                    }
                }
            });
        }
    }
}]);