angular.module('app').config(['$provide', '$routeProvider',
    function ($provide, $routeProvider) {
        $provide
            .decorator("$exceptionHandler", ["$delegate",
                function ($delegate) {
                    return function (exception, cause) {
                        console.log(exception.message);
                        $delegate(exception, cause);
                    };
                }
            ]);
        $routeProvider
            .when('/', {
                controller: 'apiController',
                templateUrl: 'pages/api.html'
            });

    }
]);