﻿using AgriMote_Api.Models;
using AgriMote_Api.Services;
using System.Web.Http;

namespace AgriMote_Api
{
    public class BoundsController : ApiController
    {
        public IHttpActionResult Get()
        {
            return Ok(Bounds.Create());
        }

        //example: /api/bounds?zoom=1&lat=1.2&lng=1.2
        [Route("~/api/bounds/{zoom=int, lat=double, lng=double}")]
        public IHttpActionResult Get(int zoom, double lat, double lng)
        {
            //1. With zoom find lat, lng find the set of tiles needed 
            //2. Once we have the tiles, each one should be divided by more tiles
            //3. each procesed and returned in a super-overlays format
            //4. decide whether to return a set of NE-SW set of images to create groundOverlay on frontEnd or what
            var b = new Bounds
            {
                zoom = zoom,
                lat = lat,
                lng = lng
            };
            var so = new SuperOverlayService(b);

            return Ok(so);
        }


    }
}
