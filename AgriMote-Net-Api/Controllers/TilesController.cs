﻿using AgriMote_Api.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Web.Http;

namespace AgriMote_Api
{
    public class TilesController : ApiController
    {
        //example: /api/tiles?fileName=B01.jp2
        [Route("~/api/tiles/{fileName=string}")]
        public IHttpActionResult Get(string fileName)
        {
            string path = @"C:\AgriDev\test-images\";
            var gs = new GInfoService(path + fileName);
            List<string> info = gs.Info();
            return Ok(info);
        }
        //example: /api/tiles
        [Route("~/api/tiles")]
        public IHttpActionResult Get()
        {
            var ts = new TileService();
            return Ok(ts.readTileFile());
        }
    }
}
