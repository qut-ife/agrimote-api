﻿using System.Web.Http;
using AgriMote_Api.Services;
using AgriMote_Api.Models;
using System.Collections.Generic;

namespace AgriMote_Api.Controllers
{
    public class TileDatesController : ApiController
    {
        //example: /api/tileDates
        [Route("~/api/tileDates")]
        public IHttpActionResult Get()
        {
            var ts = new TileDateService();
            return Ok(ts.readTileDateFile());
        }
        //example: /api/tileDates/{name}
        [Route("~/api/tileDates/{name}")]
        public IHttpActionResult Get(string name)
        {
            var ts = new TileDateService();
            var tileDates = ts.readTileDateFile();
            var t = tileDates.Find(q => q.name == name );
            return Ok(t);
        }
    }
}
