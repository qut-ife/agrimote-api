﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace AgriMote_Api.Controllers
{
    public class TileXYZController : ApiController
    {
        //example: /api/tile/0/12/34
        [Route("~/api/tile/{n}/{date}")]
        public IHttpActionResult Get(string zoom, string x, string y)
        {
            //return tile
            return Ok(" zoom: " + zoom + ", x:" + x +", y:" + y );
        }


    }
}
