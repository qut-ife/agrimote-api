﻿using AgriMote_Api;
using Microsoft.Owin;
using Microsoft.Owin.Cors;
using Microsoft.Owin.FileSystems;
using Microsoft.Owin.StaticFiles;
using Owin;
using System;
using System.IO;
using System.Web.Http;

[assembly: OwinStartup(typeof(Startup))]
namespace AgriMote_Api
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            var config = new HttpConfiguration();
            WebApiConfig.Register(config);
            
            app.UseCors(CorsOptions.AllowAll);
            app.UseWebApi(config);

            var path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Test_Data");

            app.UseStaticFiles("/sat");
        }
    }
}