﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AgriMote_Api.Models
{
    public class Tile
    {
        public string name { get; set; }
        public List<Coord> coords {get;set;}

        public List<Tile> Create()
        {
            return new List<Tile> {
                new Tile
                {
                    name = "55JFJ",
                    coords = new List<Coord>
                    {
                        new Coord { lat=-28.013801, lng=149.479980 },
                        new Coord { lat=-28.110749, lng=153.786621},
                        new Coord { lat=-25.185059, lng=153.413086},
                        new Coord { lat=-25.344026, lng=149.567871},
                    }
                }
            };
        }
    }
}