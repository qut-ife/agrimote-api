﻿using System.Collections.Generic;

namespace AgriMote_Api.Models
{
    public class Bounds
    {
        public int zoom { get; set; }

        public double lat { get; set; }

        public double lng { get; set; }

        public static List<Bounds> Create()
        {
            return new List<Bounds>
            {
                new Bounds
                {
                    zoom = 8,
                    lat = 29.123123,
                    lng = -127.123123
                },
                new Bounds
                {
                    zoom = 3,
                    lat = 22.123123,
                    lng = -122.123123
                }
            };
        }
    }
}