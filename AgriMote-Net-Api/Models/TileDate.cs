﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AgriMote_Api.Models
{
    public class TileDate
    {
        public string name { get; set; }
        public List<Dates> dates { get; set; }
    }
}