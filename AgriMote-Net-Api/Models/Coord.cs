﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AgriMote_Api.Models
{
    public class Coord
    {
        public double lat { get; set; }
        public double lng { get; set; }
    }
}