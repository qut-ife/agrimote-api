﻿using System;
using System.Collections.Generic;
using OSGeo.GDAL;
using OSGeo.OSR;

namespace AgriMote_Api
{
    public class GInfoService
    {
        string _path { get; set; }
        public List<string> info { get; set; }

        public GInfoService(string path)
        {
            _path = path;
        }
        private static string GDALInfoGetPosition(Dataset ds, double x, double y)
        {
            double[] adfGeoTransform = new double[6];
            double dfGeoX, dfGeoY;
            ds.GetGeoTransform(adfGeoTransform);

            dfGeoX = adfGeoTransform[0] + adfGeoTransform[1] * x + adfGeoTransform[2] * y;
            dfGeoY = adfGeoTransform[3] + adfGeoTransform[4] * x + adfGeoTransform[5] * y;

            return dfGeoX.ToString() + ", " + dfGeoY.ToString();
        }
        public List<string> Info()
        {
            var info = new List<string>();

            try {
                Gdal.AllRegister();

                Dataset ds = Gdal.Open(_path, Access.GA_ReadOnly);

                if (ds == null)
                {
                    throw new System.ArgumentException("Can't open " + _path);
                }

                info.Add("Raster dataset parameters:");
                info.Add("  Projection: " + ds.GetProjectionRef());
                info.Add("  RasterCount: " + ds.RasterCount);
                info.Add("  RasterSize (" + ds.RasterXSize + "," + ds.RasterYSize + ")");

                Driver drv = ds.GetDriver();

                if (drv == null)
                {
                    Console.WriteLine("Can't get driver.");
                    System.Environment.Exit(-1);
                }

                info.Add("Using driver " + drv.LongName);

                string[] metadata = ds.GetMetadata("");
                if (metadata.Length > 0)
                {
                    Console.WriteLine("  Metadata:");
                    for (int iMeta = 0; iMeta < metadata.Length; iMeta++)
                    {
                        info.Add("    " + iMeta + ":  " + metadata[iMeta]);
                    }
                    info.Add("");
                }

                metadata = ds.GetMetadata("IMAGE_STRUCTURE");
                if (metadata.Length > 0)
                {
                    Console.WriteLine("  Image Structure Metadata:");
                    for (int iMeta = 0; iMeta < metadata.Length; iMeta++)
                    {
                        info.Add("    " + iMeta + ":  " + metadata[iMeta]);
                    }
                    info.Add("");
                }

                metadata = ds.GetMetadata("SUBDATASETS");
                if (metadata.Length > 0)
                {
                    info.Add("  Subdatasets:");
                    for (int iMeta = 0; iMeta < metadata.Length; iMeta++)
                    {
                        info.Add("    " + iMeta + ":  " + metadata[iMeta]);
                    }
                    info.Add("");
                }

                metadata = ds.GetMetadata("GEOLOCATION");
                if (metadata.Length > 0)
                {
                    info.Add("  Geolocation:");
                    for (int iMeta = 0; iMeta < metadata.Length; iMeta++)
                    {
                        info.Add("    " + iMeta + ":  " + metadata[iMeta]);
                    }
                    info.Add("");
                }

                info.Add("Corner Coordinates:");
                info.Add("  Upper Left (" + GDALInfoGetPosition(ds, 0.0, 0.0) + ")");
                info.Add("  Lower Left (" + GDALInfoGetPosition(ds, 0.0, ds.RasterYSize) + ")");
                info.Add("  Upper Right (" + GDALInfoGetPosition(ds, ds.RasterXSize, 0.0) + ")");
                info.Add("  Lower Right (" + GDALInfoGetPosition(ds, ds.RasterXSize, ds.RasterYSize) + ")");
                info.Add("  Center (" + GDALInfoGetPosition(ds, ds.RasterXSize / 2, ds.RasterYSize / 2) + ")");
                info.Add("");

                string projection = ds.GetProjectionRef();
                if (projection != null)
                {
                    SpatialReference srs = new SpatialReference(null);
                    if (srs.ImportFromWkt(ref projection) == 0)
                    {
                        string wkt;
                        srs.ExportToPrettyWkt(out wkt, 0);
                        info.Add("Coordinate System is:");
                        info.Add(wkt);
                    }
                    else
                    {
                        info.Add("Coordinate System is:");
                        info.Add(projection);
                    }
                }

                if (ds.GetGCPCount() > 0)
                {
                    info.Add("GCP Projection: "+ ds.GetGCPProjection());
                    GCP[] GCPs = ds.GetGCPs();
                    for (int i = 0; i < ds.GetGCPCount(); i++)
                    {
                        info.Add("GCP[" + i + "]: Id=" + GCPs[i].Id + ", Info=" + GCPs[i].Info);
                        info.Add("          (" + GCPs[i].GCPPixel + "," + GCPs[i].GCPLine + ") -> ("
                                    + GCPs[i].GCPX + "," + GCPs[i].GCPY + "," + GCPs[i].GCPZ + ")");
                        info.Add("");
                    }
                    info.Add("");

                    double[] transform = new double[6];
                    Gdal.GCPsToGeoTransform(GCPs, transform, 0);
                    info.Add("GCP Equivalent geotransformation parameters: "+ ds.GetGCPProjection());
                    for (int i = 0; i < 6; i++)
                        info.Add("t[" + i + "] = " + transform[i].ToString());
                    info.Add("");
                }

                for (int iBand = 1; iBand <= ds.RasterCount; iBand++)
                {
                    Band band = ds.GetRasterBand(iBand);
                    info.Add("Band " + iBand + " :");
                    info.Add("   DataType: " + Gdal.GetDataTypeName(band.DataType));
                    info.Add("   ColorInterpretation: " + Gdal.GetColorInterpretationName(band.GetRasterColorInterpretation()));
                    ColorTable ct = band.GetRasterColorTable();
                    if (ct != null)
                        info.Add("   Band has a color table with " + ct.GetCount() + " entries.");

                    info.Add("   Description: " + band.GetDescription());
                    info.Add("   Size (" + band.XSize + "," + band.YSize + ")");
                    int BlockXSize, BlockYSize;
                    band.GetBlockSize(out BlockXSize, out BlockYSize);
                    info.Add("   BlockSize (" + BlockXSize + "," + BlockYSize + ")");
                    double val;
                    int hasval;
                    band.GetMinimum(out val, out hasval);
                    if (hasval != 0) info.Add("   Minimum: " + val.ToString());
                    band.GetMaximum(out val, out hasval);
                    if (hasval != 0) info.Add("   Maximum: " + val.ToString());
                    band.GetNoDataValue(out val, out hasval);
                    if (hasval != 0) info.Add("   NoDataValue: " + val.ToString());
                    band.GetOffset(out val, out hasval);
                    if (hasval != 0) info.Add("   Offset: " + val.ToString());
                    band.GetScale(out val, out hasval);
                    if (hasval != 0) info.Add("   Scale: " + val.ToString());

                    for (int iOver = 0; iOver < band.GetOverviewCount(); iOver++)
                    {
                        Band over = band.GetOverview(iOver);
                        info.Add("      OverView " + iOver + " :");
                        info.Add("         DataType: " + over.DataType);
                        info.Add("         Size (" + over.XSize + "," + over.YSize + ")");
                        info.Add("         PaletteInterp: " + over.GetRasterColorInterpretation().ToString());
                    }
                }

                
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            return info;
        }
    }
}