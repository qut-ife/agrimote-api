﻿using AgriMote_Api.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace AgriMote_Api.Services
{
    public class TileDateService
    {
        public List<TileDate> readTileDateFile()
        {
            //read configuration file
            Console.WriteLine(AppDomain.CurrentDomain.BaseDirectory);
            StreamReader r = new StreamReader(AppDomain.CurrentDomain.BaseDirectory + "tiles.dates.json");
            string json = r.ReadToEnd();
            r.Close();
            List<TileDate> tiles = JsonConvert.DeserializeObject<List<TileDate>>(json);
            return tiles;
        }
    }
}