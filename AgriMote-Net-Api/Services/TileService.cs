﻿using AgriMote_Api.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace AgriMote_Api.Services
{
    public class TileService
    {
        public List<Tile> readTileFile()
        {
            //read configuration file
            Console.WriteLine(AppDomain.CurrentDomain.BaseDirectory);
            StreamReader r = new StreamReader(AppDomain.CurrentDomain.BaseDirectory + "tiles.info.json");
            string json = r.ReadToEnd();
            r.Close();
            List<Tile> tiles = JsonConvert.DeserializeObject<List<Tile>>(json);
            return tiles;
        }
        public string GetPath(string name, string dateId, string zoom, string coordX, string coordY)
        {
            var dir = AppDomain.CurrentDomain.BaseDirectory + "/Test_Data";
            var paths = new string[6] { dir, name, dateId, zoom, coordX, coordY + ".png" };
            var path = Path.Combine(paths);
            return path;
        }
    }
}